module Helpers (ApproximatelyEqual(..)) where

class ApproximatelyEqual a where
  (=~) :: a -> a -> Bool

instance ApproximatelyEqual Double where
  (=~) a b = (a - b) < threshold
    where
      threshold = 1e-10
