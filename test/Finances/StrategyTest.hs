module Finances.StrategyTest
  ( strategySpec
  ) where


import           Data.Ratio                ((%))
import           Test.Hspec

import           Finances.Strategy         (applyStrategy, balance,
                                            beginningAtAge, contributeEqually,
                                            drawEqually, drawProportionately,
                                            endingAtAge, (+++))
import           Finances.Types.Holdings   (Holding (..), Asset (..), TaxStatus (..))
import           Finances.Types.Simulation (SimulationState (..), defaultState)

strategySpec :: Spec
strategySpec =
  describe "Strategy" $ do
    describe "beginningAtAge" $ do
      it "does not apply a strategy before a given age" $ do
        let state = defaultState { age = 50, lifeExpectancy = 50, holdings = [] }
        let strategy = beginningAtAge 60 (const [Holding CapitalGains USStocks 50])
        applyStrategy strategy state `shouldBe` []

      it "does apply a strategy at a given age" $ do
        let state = defaultState { age = 50, lifeExpectancy = 50, holdings = [] }
        let strategy = beginningAtAge 50 (const [Holding CapitalGains USStocks 50])
        applyStrategy strategy state `shouldBe` [Holding CapitalGains USStocks 50]

      it "does apply a strategy after a given age" $ do
        let state = defaultState { age = 50, lifeExpectancy = 50, holdings = [] }
        let strategy = beginningAtAge 40 (const [Holding CapitalGains USStocks 50])
        applyStrategy strategy state `shouldBe` [Holding CapitalGains USStocks 50]

    describe "endingAtAge" $ do
      it "does apply a strategy before a given age" $ do
        let state = defaultState { age = 50, lifeExpectancy = 50, holdings = [] }
        let strategy = endingAtAge 60 (const [Holding CapitalGains USStocks 50])
        applyStrategy strategy state `shouldBe` [Holding CapitalGains USStocks 50]

      it "does not apply a strategy at a given age" $ do
        let state = defaultState { age = 50, lifeExpectancy = 50, holdings = [] }
        let strategy = endingAtAge 50 (const [Holding CapitalGains USStocks 50])
        applyStrategy strategy state `shouldBe` []

      it "does not apply a strategy after a given age" $ do
        let state = defaultState { age = 50, lifeExpectancy = 50, holdings = [] }
        let strategy = endingAtAge 40 (const [Holding CapitalGains USStocks 50])
        applyStrategy strategy state `shouldBe` []

    describe "applyStrategy" $ do
      it "eliminates single holdings that have been drawn to zero" $
        applyStrategy (const [Holding CapitalGains USStocks 0]) defaultState `shouldBe` []

      it "does not eliminate single holdings that have not been drawn to zero" $
        applyStrategy (const [Holding CapitalGains USStocks 1]) defaultState `shouldBe` [Holding CapitalGains USStocks 1]

      it "trues up holdings that have been drawn below zero" $
        applyStrategy (const [Holding CapitalGains USStocks (negate 50), Holding CapitalGains USBonds 100]) defaultState `shouldBe` [Holding CapitalGains USBonds 50]

      it "eliminates all holdings when drawn below zero" $
        applyStrategy (const [Holding CapitalGains USStocks (negate 500), Holding CapitalGains USBonds 100]) defaultState `shouldBe` []

    describe "drawProportionately" $ do
      it "should draw in proportion to the asset allocation (50/50)" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 500, Holding CapitalGains USBonds 500] }
        applyStrategy (drawProportionately 100) state `shouldBe` [Holding CapitalGains USStocks 450, Holding CapitalGains USBonds 450]

      it "should draw in proportion to the asset allocation (30/70)" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 300, Holding CapitalGains USBonds 700] }
        applyStrategy (drawProportionately 100) state `shouldBe` [Holding CapitalGains USStocks 270, Holding CapitalGains USBonds 630]

    describe "drawEqually" $ do
      it "should draw an equal amount from each holding (50/50)" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 500, Holding CapitalGains USBonds 500] }
        applyStrategy (drawEqually 100) state `shouldBe` [Holding CapitalGains USStocks 450, Holding CapitalGains USBonds 450]

      it "should draw an equal amount from each holding (30/70)" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 300, Holding CapitalGains USBonds 700] }
        applyStrategy (drawEqually 100) state `shouldBe` [Holding CapitalGains USStocks 250, Holding CapitalGains USBonds 650]

    describe "contributeEqually" $ do
      it "should add an equal amount to each holding (50/50)" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 500, Holding CapitalGains USBonds 500] }
        applyStrategy (contributeEqually 100) state `shouldBe` [Holding CapitalGains USStocks 550, Holding CapitalGains USBonds 550]

      it "should add an equal amount to each holding (30/70)" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 300, Holding CapitalGains USBonds 700] }
        applyStrategy (contributeEqually 100) state `shouldBe` [Holding CapitalGains USStocks 350, Holding CapitalGains USBonds 750]

    describe "balance" $
      it "should balance allocation" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 500, Holding CapitalGains USBonds 500] }
        applyStrategy (balance [(USStocks, 70 % 100), (USBonds, 30 % 100)]) state `shouldBe` [Holding CapitalGains USStocks 700, Holding CapitalGains USBonds 300]

    describe "(+++)" $ do
      it "should combine strategies" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 500, Holding CapitalGains USBonds 500] }
        let strategy = contributeEqually 100 +++ contributeEqually 100
        applyStrategy strategy state `shouldBe` [Holding CapitalGains USStocks 600, Holding CapitalGains USBonds 600]

      it "should combine strategies with balancing" $ do
        let state = defaultState { holdings = [Holding CapitalGains USStocks 500, Holding CapitalGains USBonds 500] }
        let strategy = endingAtAge 50 (contributeEqually 1000) +++ beginningAtAge 50 (drawEqually 100) +++ balance [(USStocks, 6 % 10), (USBonds, 4 % 10)]
        applyStrategy strategy state { age = 30 } `shouldBe` [Holding CapitalGains USStocks 1200, Holding CapitalGains USBonds 800]
        applyStrategy strategy state { age = 60 } `shouldBe` [Holding CapitalGains USStocks 540, Holding CapitalGains USBonds 360]
