module Finances.HoldingsTest
  ( holdingsSpec
  ) where

import           Finances.Holdings  (total)
import           Finances.Types.Holdings  (Holding (..), Asset (..), TaxStatus (..))
import           Test.Hspec

holdingsSpec :: Spec
holdingsSpec =
  describe "Holdings"
    totalSpec

totalSpec :: Spec
totalSpec =
  describe "total" $ do
    it "finds the total value of a collection of holdings" $
      total [Holding CapitalGains USBonds 100, Holding CapitalGains USStocks 100] `shouldBe` 200

    it "returns zero when no holdings are present" $
      total [] `shouldBe` 0
