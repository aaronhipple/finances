module Finances.SimulationTest
  ( simulationSpec
  ) where


import           Data.Foldable             (toList)
import qualified Money
import           Test.Hspec

import           Finances.Simulation       (simulate)
import           Finances.Types.Holdings   (Holding (..), Asset (..), TaxStatus (..))
import           Finances.Types.Simulation (SimulationResults (..),
                                            SimulationState (..), defaultState)

simulationSpec :: Spec
simulationSpec =
  describe "Simulation"
    simulateSpec

simulateSpec :: Spec
simulateSpec =
  describe "simulate" $ do
    it "produces a value for each year simulated" $ do
      simulated <- simulate 1 holdings state
      length (valuesByYear simulated) `shouldBe` (1 + lifeExpectancy state - age state)

    it "produces as many values in a series as simulations that were run" $ do
      simulated1 <- simulate 1 holdings state
      length (head $ toList $ valuesByYear simulated1) `shouldBe` 1
      simulated5 <- simulate 5 holdings state
      length (head $ toList $ valuesByYear simulated5) `shouldBe` 5

    it "gives a sensible success rate" $ do
      simulated <- simulate 5 holdings state
      let successfulScenarios = length $ filter (> Money.dense' 0) $ toList $ last $ toList $ valuesByYear simulated
      5 * successRate simulated `shouldBe` fromIntegral successfulScenarios

    it "gives a sensible success rate when everything fails" $ do
      simulated <- simulate 5 (const []) state
      successRate simulated `shouldBe` 0


state :: SimulationState
state = defaultState
  { holdings = [ Holding CapitalGains USStocks 100
               , Holding CapitalGains USBonds 100
               ]
  , age = 30
  , lifeExpectancy = 85
  }
