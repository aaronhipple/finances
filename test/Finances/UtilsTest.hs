module Finances.UtilsTest
  ( utilsSpec
  ) where

import           Control.Monad   (forM_)
import           Finances.Utils  (mean, percentile, stdev, variance)
import           Helpers         (ApproximatelyEqual (..))
import           Test.Hspec
import           Test.QuickCheck

utilsSpec :: Spec
utilsSpec =
  describe "Utils" $ do
    meanSpec
    varianceSpec
    stdevSpec
    percentileSpec

meanSpec :: Spec
meanSpec =
  describe "mean" $ do
    let findsMean (samples, expected) = it ("finds the mean of samples " ++ show samples) $ mean samples `shouldBe` expected
    forM_
      [ ([1, 2, 3], 2 :: Double)
      , ([0, 2, 4], 2 :: Double)
      , ([-1, 2, 5], 2 :: Double)
      ]
      findsMean

    it "subtracted from samples and summed equals zero" $
      property summedSamplesWithMeanEqualsZero

    it "increases by `n` when each sample increases by `n`" $
      property $ changesWithArithmetic (+)
    it "decreases by `n` when each sample decreases by `n`" $
      property $ changesWithArithmetic (-)
    it "multiplies by `n` when each sample multiplies by `n`" $
      property $ changesWithArithmetic (*)
    it "is divided by `n` when each sample is divided by `n`" $
      property $ changesWithArithmetic (/)

summedSamplesWithMeanEqualsZero :: NonEmptyList Double -> Bool
summedSamplesWithMeanEqualsZero (NonEmpty xs) = sum (map f xs) =~ 0
  where
    f x = x - avg
    avg = mean xs

changesWithArithmetic :: (Double -> Double -> Double) -> NonZero Double -> NonEmptyList Double -> Bool
changesWithArithmetic op (NonZero amount) (NonEmpty xs) = original =~ altered
  where
    f x = x `op` amount
    original = f (mean xs)
    altered = mean (map f xs)

varianceSpec :: Spec
varianceSpec =
  describe "variance" $ do
    let findsVariance (samples, expected) = it ("finds the variance of samples " ++ show samples) $ variance samples `shouldBe` expected
    forM_
      [ ([1, 2, 3], 1 :: Double)
      , ([0, 2, 4], 4 :: Double)
      , ([-1, 2, 5], 9 :: Double)
      ] findsVariance

stdevSpec :: Spec
stdevSpec =
  describe "stdev" $ do
    let findsStdev (samples, expected) = it ("finds the variance of samples " ++ show samples) $ stdev samples `shouldBe` expected
    forM_
      [ ([1, 2, 3], 1 :: Double)
      , ([0, 2, 4], 2 :: Double)
      , ([-1, 2, 5], 3 :: Double)
      ] findsStdev

percentileSpec :: Spec
percentileSpec =
  describe "percentile" $ do
    it "finds the 95th percentile of samples from 1 to 100" $
      percentile 95 [1..100] `shouldBe` (95 :: Double)

    it "finds the 99th percentile of samples from 1 to 100" $
      percentile 99 [1..100] `shouldBe` (99 :: Double)

    it "finds the 95th percentile of samples from 1 to 1000" $
      percentile 95 [1..1000] `shouldBe` (950 :: Double)

    it "finds the 99th percentile of samples from 1 to 1000" $
      percentile 99 [1..1000] `shouldBe` (990 :: Double)
