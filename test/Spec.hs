import           Test.Hspec

import           Finances.HoldingsTest   (holdingsSpec)
import           Finances.SimulationTest (simulationSpec)
import           Finances.StrategyTest   (strategySpec)
import           Finances.UtilsTest      (utilsSpec)

main :: IO ()
main = hspec $
  describe "Finances" $ do
    holdingsSpec
    simulationSpec
    strategySpec
    utilsSpec
