module Lib
    ( someFunc
    ) where

import           Control.Monad             (mapM_)
import           Data.Ratio                ((%))

import           Finances.Simulation       (simulate)
import           Finances.Strategy
import           Finances.Types.Holdings   (Asset (..), Holding (..),
                                            TaxStatus (..))
import           Finances.Types.Simulation (SimulationState (..), defaultState)
import           Finances.Types.Strategy   (Strategy)

someFunc :: IO ()
someFunc = mapM_ runSamples [35 .. 50]

runSamples :: Int -> IO ()
runSamples retireAge = mapM_ (runSample retireAge) [0, 5 .. 15]

runSample :: Int -> Int -> IO ()
runSample retireAge coastPeriod = do
  putStrLn $ "Coast (90/10) at " ++ show retireAge ++ " for " ++ show coastPeriod ++ " year(s): "
  print =<< simulate nSamples (coastAtAgeForYears retireAge coastPeriod +++ aggressiveBalance +++ payFees fees) state
  putStrLn $ "Coast (60/40) at " ++ show retireAge ++ " for " ++ show coastPeriod ++ " year(s): "
  print =<< simulate nSamples (coastAtAgeForYears retireAge coastPeriod +++ moderateBalance +++ payFees fees) state
  putStrLn $ "Coast (30/70) at " ++ show retireAge ++ " for " ++ show coastPeriod ++ " year(s): "
  print =<< simulate nSamples (coastAtAgeForYears retireAge coastPeriod +++ conservativeBalance +++ payFees fees) state

nSamples :: Int
nSamples = 2500

annualBudget :: Num a => a
annualBudget = 50000

annualSavings :: Num a => a
annualSavings = 50000

fees :: Asset -> Rational
fees Cash = 0
fees USBonds = 5 % 1000
fees USStocks = 5 % 1000
fees WorldStocks = 5 % 1000
fees WorldBonds = 5 % 1000

coastAtAgeForYears :: Int -> Int -> Strategy
coastAtAgeForYears coastAge years =
  endingAtAge coastAge (contributeEqually annualSavings) +++
  beginningAtAge (coastAge + years) (drawProportionately annualBudget)

aggressiveBalance :: Strategy
aggressiveBalance = balance [(USStocks, 9 % 10), (USBonds, 1 % 10)]

moderateBalance :: Strategy
moderateBalance = balance [(USStocks, 6 % 10), (USBonds, 4 % 10)]

conservativeBalance :: Strategy
conservativeBalance = balance [(USStocks, 3 % 10), (USBonds, 7 % 10)]

-- | A sample starting state.
state :: SimulationState
state = defaultState
  { holdings =
    [ Holding Income Cash 50000
    ]
  , age = 25
  , lifeExpectancy = 90
  }
