{-# LANGUAGE DataKinds #-}
module Finances.Strategy (drawProportionately, drawEqually, applyStrategy, beginningAtAge, endingAtAge, contributeEqually, (+++), balance, payFees) where

import           Data.List                 (partition)
import           Data.Ratio                (denominator, numerator, (%))
import qualified Money

import           Finances.Holdings         (total)
import           Finances.Types.Holdings   (Holding (..), Asset (..), TaxStatus (..))
import           Finances.Types.Simulation (SimulationState (..))
import           Finances.Types.Strategy   (Strategy)

(+++) :: Strategy -> Strategy -> Strategy
(+++) f g state = g intermediate
  where
    intermediate = state { holdings = f state }

beginningAtAge :: Int -> Strategy -> Strategy
beginningAtAge n f state
  | age state >= n = f state
  | otherwise = holdings state

endingAtAge :: Int -> Strategy -> Strategy
endingAtAge n f state
  | age state < n = f state
  | otherwise = holdings state

contributeEqually :: Money.Dense "USD" -> Strategy
contributeEqually n state = contribute <$> holdings state
  where
    contribute (Holding tax asset value) = Holding tax asset $ value + (n `unsafeDiv` count)
    count = fromIntegral $ length $ holdings state

drawProportionately :: Money.Dense "USD" -> Strategy
drawProportionately n state = draw <$> holdings state
  where
    draw (Holding tax asset value) = Holding tax asset $ value - (n * Money.dense' (toRational value / toRational total'))
    total' = total $ holdings state

drawEqually :: Money.Dense "USD" -> Strategy
drawEqually n state = draw <$> holdings state
  where
    draw (Holding tax asset value) = Holding tax asset $ value - (n `unsafeDiv` count)
    count = fromIntegral $ length $ holdings state

balance :: [(Asset, Rational)] -> Strategy
balance ratios state = map f ratios
  where
    tax = CapitalGains
    f (asset, ratio) = Holding tax asset (Money.dense' (ratio * toRational total'))
    total' = total $ holdings state

payFees :: (Asset -> Rational) -> Strategy
payFees fee state = map f (holdings state)
  where
    f (Holding tax asset value ) = Holding tax asset (Money.dense' (toRational value * (1 - fee asset)))

applyStrategy :: Strategy -> SimulationState -> [Holding]
applyStrategy strategy state
  | debt > total good = []
  | otherwise = map drawGood good
  where
    (good, bad) = partition f (strategy state)
    debt = negate $ total bad
    f (Holding _ _ value) = value > 0
    drawGood (Holding tax asset value) = Holding tax asset $ value - (debt `unsafeDiv` count)
      where
        count = fromIntegral $ length good

unsafeDiv :: Money.Dense b -> Integer -> Money.Dense b
unsafeDiv n d = amount
  where
    amount = Money.dense' (num % denom)
    num = numerator $ toRational n
    denom = denominator (toRational n) * d
