{-# LANGUAGE DataKinds #-}
module Finances.Holdings (randomInflation, randomRate, total) where

import           Data.Random.Normal            (normal')
import           System.Random                 (RandomGen)

import qualified Money

import qualified Finances.Data.SP500           as SP500
import qualified Finances.Data.USInflation     as USInflation
import qualified Finances.Data.USTreasuryBonds as USTreasuryBonds
import           Finances.Types.Holdings       (Holding (..), Asset (..))

-- | Find the total value from a set of holdings.
total :: [Holding] -> Money.Dense "USD"
total hs = case hs of
  []  -> 0
  hs' -> sum $ value <$> hs'

value :: Holding -> Money.Dense "USD"
value (Holding _ _ x) = x

-- | Generate a random rate for a given `Asset`.
randomRate :: RandomGen g => Asset -> g -> (Double, g)
randomRate Cash = normal' (0, 0)
randomRate USStocks = normal' (SP500.rateMean, SP500.rateStdev)
randomRate USBonds = normal' (USTreasuryBonds.rateMean, USTreasuryBonds.rateStdev)
-- TODO get real historical data for this
randomRate WorldStocks = normal' (SP500.rateMean, SP500.rateStdev)
-- TODO get real historical data for this
randomRate WorldBonds = normal' (USTreasuryBonds.rateMean, USTreasuryBonds.rateStdev)

-- | Generate a random rate for inflation.
randomInflation :: RandomGen g => g -> (Double, g)
randomInflation = normal' (USInflation.rateMean, USInflation.rateStdev)

