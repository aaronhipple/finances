module Finances.Utils
  ( mean
  , variance
  , stdev
  , percentile
  )
  where

import           Data.Foldable (toList)

mean :: (Fractional a, Foldable t) => t a -> a
mean xs = total / n
  where
    total = sum xs
    n = fromIntegral $ length xs

variance :: (Foldable t, Functor t, Fractional a) => t a -> a
variance xs = sumOfSquares / (n - 1)
  where
    avg = mean xs
    f x = (x - avg) ^ (2 :: Integer)
    sumOfSquares = sum $ fmap f xs
    n = fromIntegral $ length xs

stdev :: (Foldable t, Functor t, Floating a) => t a -> a
stdev = sqrt . variance

-- At present assumes the input is sorted.
-- `length` and indexing are O(n). Make this better.
percentile :: Foldable t => Int -> t a -> a
percentile p xs' = xs !! i
  where
    xs = toList xs'
    i = ceiling (n * (fromIntegral p / 100)) - 1

    n :: Double
    n = fromIntegral $ length xs
