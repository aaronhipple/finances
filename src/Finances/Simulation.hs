{-# LANGUAGE DataKinds #-}
module Finances.Simulation (simulate) where

import           Control.Monad             (replicateM)
import           Control.Monad.Loops       (iterateUntilM)
import           Data.Foldable             (toList)
import           Data.List                 (transpose)
import           Data.Sequence             (Seq (..))
import qualified Data.Sequence             as Seq
import qualified Money
import           System.Random             (getStdRandom)

import           Finances.Holdings         (randomInflation, randomRate, total)
import           Finances.Strategy         (applyStrategy)
import           Finances.Types.Holdings   (Holding (..))
import           Finances.Types.Simulation (SimulationResults (..),
                                            SimulationState (..))
import           Finances.Types.Strategy   (Strategy)

-- | Run a Monte Carlo simulation (`n` iterations).
simulate :: Int -> Strategy -> SimulationState -> IO SimulationResults
simulate n strategy initial = do
  results <- replicateM n (simulateOne strategy initial)
  return $ resultsFromStates results

-- | Run a single iteration of a simulation.
simulateOne :: Strategy -> SimulationState -> IO SimulationState
simulateOne strategy = iterateUntilM (\s -> age s >= lifeExpectancy s) (step strategy)

-- | Run a single step of the simulation.
step :: Strategy -> SimulationState -> IO SimulationState
step strategy state = do
  inflation <- getStdRandom randomInflation
  newHoldings <- mapM (grow inflation) $ applyStrategy strategy state
  return state { holdings = newHoldings
               , values = values state :|> total (holdings state)
               , age = age state + 1
               , failed = null newHoldings
               }

-- | Produce a set of results from a collection of end states.
resultsFromStates :: (Foldable t, Functor t) => t SimulationState -> SimulationResults
resultsFromStates states = SimulationResults { valuesByYear = valuesByYear', successRate = successRate' }
  where
    valuesByYear' = Seq.fromList $ fmap Seq.fromList valuesByYear''
    valuesByYear'' = transpose $ toList $ fmap collectValues states
    successRate' = success / n
    (n, success) = foldl count (0, 0) states
    count (n', success') s = (n' + 1, if failed s then success' else success' + 1)
    collectValues :: SimulationState -> [Money.Dense "USD"]
    collectValues state = toList $ values state :|> total (holdings state)

-- | Return the new nominal value of a holding with random growth.
grow :: Double -> Holding -> IO Holding
grow inflation (Holding tax asset value) = do
  rate <- getStdRandom $ randomRate asset
  let nominalRate = rate - inflation
  return $ Holding tax asset (Money.dense' (toRational value * (1 + toRational nominalRate)))
