{-# LANGUAGE DataKinds #-}
module Finances.Types.Simulation (SimulationState(..), SimulationResults(..), defaultState) where

import           Data.List               (intercalate)
import           Data.Ratio              (approxRational, numerator)
import           Data.Sequence           (Seq (..), (!?))
import qualified Data.Sequence           as Seq
import qualified Money

import           Finances.Holdings       (total)
import           Finances.Types.Holdings (Holding)
import           Finances.Utils          (percentile, mean, stdev)

data SimulationState = SimulationState
  { holdings       :: [Holding]
  , values         :: Seq (Money.Dense "USD")
  , age            :: Int
  , lifeExpectancy :: Int
  , failed         :: Bool
  }

data SimulationResults = SimulationResults
  { valuesByYear :: Seq (Seq (Money.Dense "USD"))
  , successRate  :: Double
  }

instance Show SimulationResults where
  show results =
    unlines $ [ "success rate: " ++ show (successRate results)
              , "mean: " ++ show (mean . fmap realToFrac <$> lastYear :: Maybe Double)
              , "stdev: " ++ show (stdev . fmap realToFrac <$> lastYear :: Maybe Double)
              ] ++ map p [95, 80 .. 0]
    where
      p n = "p" ++ show n ++ ": " ++ show (fmap (showMeTheMoney . percentile n) lastYear)
      lastYear = sortedYearlyValues !? (y - 1)
      y = Seq.length $ valuesByYear results
      sortedYearlyValues = Seq.unstableSort <$> valuesByYear results

showMeTheMoney :: Money.Dense "USD" -> String
showMeTheMoney amount = "$" ++ commafy (show dollars)
  where
    dollars = numerator $ toRational $ simplify amount
    commafy xs
      | length xs < 4 = xs
      | otherwise = reverse $ intercalate "," $ split3 $ reverse xs
    split3 []         = [[]]
    split3 [a]        = [[a]]
    split3 [a, b]     = [[a, b]]
    split3 [a, b, c]  = [[a, b, c]]
    split3 (a:b:c:ds) = [a, b, c] : split3 ds


simplify :: Money.Dense "USD" -> Money.Dense "USD"
simplify = Money.dense' . flip approxRational 1 . toRational

instance Show SimulationState where
  show state = unlines
    [ "SimulationState {"
    , "age = " ++ show (age state)
    , "total = " ++ show (total $ holdings state)
    , "holdings = " ++ show (holdings state)
    , "}"
    ]

defaultState :: SimulationState
defaultState = SimulationState
  { holdings = []
  , values = Seq.empty
  , age = 0
  , lifeExpectancy = 100
  , failed = False
  }
