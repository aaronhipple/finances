{-# LANGUAGE DataKinds #-}
module Finances.Types.Holdings (Holding(..), TaxStatus (..), Asset (..)) where

import qualified Money

data Asset
  = Cash
  | USStocks
  | USBonds
  | WorldStocks
  | WorldBonds
  deriving (Show, Eq, Enum)

data TaxStatus
  = CapitalGains
  | Income
  | TaxFree
  deriving (Show, Eq, Enum)

data Holding = Holding TaxStatus Asset (Money.Dense "USD")
  deriving (Show, Eq)
