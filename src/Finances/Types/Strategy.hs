module Finances.Types.Strategy (Strategy) where

import           Finances.Types.Holdings   (Holding)
import           Finances.Types.Simulation (SimulationState)

type Strategy = SimulationState -> [Holding]
